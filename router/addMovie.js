const db = require('../db');

module.exports = (req, res) => {
  const { id, avFrom, avUntil } = req.body;
  const addMovieRequest = db.addMovie(id, avFrom, avUntil);
  addMovieRequest
    .then((movie) => {
      if (!movie) {
        res.set('application/json');
        res.status(409);
        res.send({ Info: 'Movie is already in the database!' });
      } else if (movie.Error) {
        res.set('application/json');
        res.status(409);
        res.send(movie);
      } else {
        res.set('application/json');
        res.status(201);
        res.send(movie);
      }
    })
    .catch((err) => {
      console.log(err);
      res.send(err);
    });
};
