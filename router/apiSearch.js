const helper = require('../Helper');

module.exports = (req, res) => {
  const { t } = req.query;
  const getMovie = helper.getMovie(t);
  getMovie
    .then((ApiResponse) => {
      res.type('application/json');
      res.set(200);
      res.send(ApiResponse.data);
    })
    .catch((requestError) => {
      res.type('application/json');
      res.set(409);
      res.send(requestError);
    });
};
