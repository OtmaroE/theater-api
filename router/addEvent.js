const helper = require('../Helper');
const db = require('../db');

module.exports = (req, res) => {
  const testStartDate = helper.validateDate(req.body.Start);
  if (testStartDate) {
    const jsonRequest = req.body;
    const newEventRequest = db.addEvent(jsonRequest);
    newEventRequest
      .then((newEvent) => {
        res.set('application/json');
        res.status(201);
        res.send(newEvent);
      })
      .catch((failedInfo) => {
        res.set('application/json');
        res.status(409);
        res.send({ Error: failedInfo.message });
      });
  } else {
    res.send('Bad JSON, please refer to documentation to see proper request');
  }
};
