const express = require('express');

const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: false }));

const auth = require('../auth');

const apiSearch = require('./apiSearch');
const billboard = require('./billboardMovies');
const addMovie = require('./addMovie');
const movieSchedule = require('./movieSchedule');
const addEvent = require('./addEvent');
const buyTicket = require('./buyTicket');
const getTickets = require('./getTickets');
const login = require('./login');
const addUser = require('./addUser');
const addRoom = require('./addRoom');

app.get('/movies/dbapi', (req, res) => auth.authAdmin(req, res, apiSearch));
app.get('/movies', billboard);
app.post('/movies', (req, res) => auth.authAdmin(req, res, addMovie));
app.get('/movies/:id', movieSchedule);
app.post('/schedules', addEvent);
app.post('/tickets', buyTicket);
app.get('./tickets', getTickets);
app.post('/users/login', (req, res) => auth.generateToken(req, res, login));
app.post('/users', (req, res) => auth.authAdmin(req, res, addUser));
app.post('/users/singup', (req, res) => addUser(req, res, 'customer'));
app.post('/users/logout', (req, res) => auth.removeToken(req, res));
app.post('/rooms', (req, res) => auth.authAdmin(req, res, addRoom));

module.exports = app;
