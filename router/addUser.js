const db = require('../db');

module.exports = (req, res, userType) => {
  const JsonData = req.body;
  if (userType) {
    JsonData.Role = userType;
  }
  const addUserReq = db.addUser(JsonData);
  addUserReq
    .then((newUser) => {
      if (!newUser) {
        res.type('application/json');
        res.status(409);
        res.send({ Info: 'User already Exists!' });
      } else {
        res.type('application/json');
        res.status(201);
        res.send(newUser);
      }
    })
    .catch((mongoDbError) => {
      res.type('application/json');
      res.status(409);
      res.send(mongoDbError);
    });
};
