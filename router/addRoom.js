const db = require('../db');

module.exports = (req, res) => {
  const JsonRequestInfo = req.body;

  const addRoomRequest = db.addRoom(JsonRequestInfo);
  addRoomRequest
    .then((newRoom) => {
      if (!newRoom) {
        res.set('application/json');
        res.status(409);
        res.send({ Error: 'Wrong Request. Make sure you have a Name field!' });
      } else if (newRoom === 409) {
        res.set('application/json');
        res.status(409);
        res.send({ Info: 'Room already exists!' });
      } else {
        res.set('appliction/json');
        res.status(201);
        res.send(newRoom);
      }
    })
    .catch((mongoDBError) => {
      res.set('application/json');
      res.status(500);
      res.send(mongoDBError);
    });
};
