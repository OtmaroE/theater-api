require('dotenv').config();
const jwt = require('jsonwebtoken');
const db = require('../db');

const { secret } = process.env;

function generateToken(req, res) {
  const { username } = req.body;
  const { password } = req.body;

  const userRequest = db.getUser(username);
  userRequest
    .then((user) => {
      if (user === null) {
        res.type('application/json');
        res.status(409);
        res.send({ Error: 'The username does not exists!' });
      } else if (user.Password === password) {
        const payload = { id: user.Username, role: user.Role };
        const token = jwt.sign(payload, secret, { expiresIn: '1h' });
        const addToken = db.addToken(token);
        addToken
          .then(() => {
            res.type('application/json');
            res.status(201);
            res.send({ Token: token });
          });
      } else {
        res.type('application/json');
        res.status(409);
        res.send({ Error: 'The password is incorrect!' });
      }
    })
    .catch((mongoDbError) => {
      res.type('application/json');
      res.status(500);
      res.send(mongoDbError);
    });
}
function removeToken(req, res) {
  const { authorization } = req.headers;
  const token = authorization.split(' ')[1];

  const delTokenRequest = db.delToken(token);
  delTokenRequest
    .then((mongoDbResponse) => {
      res.send(mongoDbResponse);
    })
    .catch((mongoDbError) => {
      res.type('application/json');
      res.status(500);
      res.send(mongoDbError);
    });
}
function authAdmin(req, res, next) {
  const { authorization } = req.headers;
  if (authorization === undefined) {
    res.type('application/json');
    res.status(409);
    res.send({ Error: 'Please log in first' });
  } else {
    const token = authorization.split(' ')[1];
    jwt.verify(token, secret, (error, user) => {
      if (error) {
        res.type('application/json');
        res.status(401);
        res.send({ Error: error });
      } else if (user.role === 'admin') {
        req.user = user;
        next(req, res, next);
      } else {
        res.type('application/json');
        res.status(403);
        res.send({ Error: 'You are not an admin!' });
      }
    });
  }
}

function authEmployee(req, res, next) {
  const { authorization } = req.headers;
  if (authorization === undefined) {
    res.type('application/json');
    res.status(409);
    res.send({ Error: 'Please log in first' });
  } else {
    const token = authorization.split(' ')[1];
    jwt.verify(token, secret, (error, user) => {
      if (error) {
        res.type('application/json');
        res.status(401);
        res.send({ Error: error });
      } else if (user.role !== 'user') {
        req.user = user;
        next(req, res, next);
      } else {
        res.type('application/json');
        res.status(403);
        res.send({ Error: 'You need to work for the movie theater!' });
      }
    });
  }
}

function authUser(req, res, next) {
  const { authorization } = req.headers;
  if (authorization === undefined) {
    res.type('application/json');
    res.status(409);
    res.send({ Error: 'Please log in first' });
  } else {
    const token = authorization.split(' ')[1];
    jwt.verify(token, secret, (error, user) => {
      if (error) {
        res.type('application/json');
        res.status(401);
        res.send({ Error: error });
      } else {
        req.user = user;
        next(req, res, next);
      }
    });
  }
}
module.exports = {
  generateToken,
  removeToken,
  authAdmin,
  authEmployee,
  authUser,
};
