require('dotenv').config();

const { PORT } = process.env;

const app = require('./router');


app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
