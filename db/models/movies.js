const mongoose = require('mongoose');

const movies = mongoose.Schema({
  Title: String,
  IMDB: String,
  Rated: String,
  Runtime: Number,
  Genre: String,
  Director: String,
  Actors: String,
  Language: String,
  Country: String,
  Poster: String,
  Type: String,
  Production: String,
  AvailableFrom: Date,
  AvailableUntil: Date,
});

const Movies = mongoose.model('Movies', movies);

module.exports = {
  Movies,
  movies,
};
