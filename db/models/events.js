const mongoose = require('mongoose');

const { Movies } = require('./movies.js');

const { Rooms } = require('./rooms.js');

const events = mongoose.Schema({
  Code: String,
  Room: { type: mongoose.Schema.Types.String, ref: Rooms },
  Start: {
    type: Date,
    index: true,
  },
  MovieEnd: Date,
  EventEnd: {
    type: Date,
    index: true,
  },
  MovieId: { type: mongoose.Schema.Types.String, ref: Movies },
  Price: Number,
  Seats: Number,
  Note: String,
});

const Events = mongoose.model('Events', events);

module.exports = {
  events,
  Events,
};
