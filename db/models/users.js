const mongoose = require('mongoose');

const users = mongoose.Schema({
  Fname: String,
  Lname: String,
  Username: {
    type: String,
    required: true,
  },
  Password: {
    type: String,
    required: true,
  },
  Role: {
    type: String,
    required: true,
  },
});

const Users = mongoose.model('Users', users);

module.exports = {
  users,
  Users,
};
