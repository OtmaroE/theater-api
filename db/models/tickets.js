const mongoose = require('mongoose');

const { Events } = require('./events.js');

const events = mongoose.Schema({
  Type: String,
  Issued: Date,
  Event: { type: mongoose.Schema.Types.ObjectId, ref: Events },
});

module.exports = events;
