const mongoose = require('mongoose');

const tokens = mongoose.Schema({
  Token: String,
});

const Tokens = mongoose.model('token', tokens);

module.exports = {
  Tokens,
};
