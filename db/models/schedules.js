const mongoose = require('mongoose');

const { Events } = require('./events.js');

const schedule = mongoose.Schema({
  Date,
  Events: [{ type: mongoose.Schema.Types.ObjectId, ref: Events }],
});

const Schedule = mongoose.model('Schedule', schedule);

module.exports = {
  schedule,
  Schedule,
};
