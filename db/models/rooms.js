const mongoose = require('mongoose');

const rooms = mongoose.Schema({
  Name: {
    type: String,
    required: true,
  },
  Description: String,
  Seats: Number,
  Price: Number,
  Available: Boolean,
});

const Rooms = mongoose.model('Rooms', rooms);

module.exports = {
  rooms,
  Rooms,
};
