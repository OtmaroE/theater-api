const mongoose = require('mongoose');
const helper = require('../Helper');

mongoose.connect('mongodb://localhost:27017/theater', { autoindex: false });

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));

db.once('open', () => {
  console.log('Connection stablish');
});

const { Users } = require('./models/users');
const { Tokens } = require('./models/tokens');
const { Movies } = require('./models/movies');
const { Rooms } = require('./models/rooms');
const { Events } = require('./models/events');

const getUser = (username) => {
  const user = Users.findOne({ Username: username });
  return user;
};

const addUser = (JsonData) => {
  const newUser = getUser(JsonData.username)
    .then((User) => {
      if (User === null) {
        return Users.create({
          Fname: JsonData.fname,
          Lname: JsonData.lname,
          Username: JsonData.username,
          Password: JsonData.password,
          Role: JsonData.role,
        });
      }
      return null;
    });
  return newUser;
};

const addToken = (token) => {
  const tokenInsert = Tokens.create({ Token: token });
  return tokenInsert;
};

const delToken = (token) => {
  const tokenDelete = Tokens.deleteOne({ Token: token });
  return tokenDelete;
};

const addMovie = (movieId, availableFrom, availableUnitl) => {
  const movie = helper.getMovieById(movieId);
  return movie
    .then((ApiResponse) => {
      const movieInfo = ApiResponse.data;
      return movieInfo;
    })
    .then((movieInfo) => {
      const findMovie = Movies.findOne({ IMDB: movieId });
      return Promise.all([findMovie, movieInfo]);
    })
    .then(([findMovie, movieInfo]) => {
      if (!findMovie && !movieInfo.Error) {
        const createMovie = Movies.create({
          Title: movieInfo.Title,
          IMDB: movieInfo.imdbID,
          Rated: movieInfo.Rated,
          Runtime: movieInfo.Runtime.split(' ')[0],
          Genre: movieInfo.Genre,
          Director: movieInfo.Director,
          Actors: movieInfo.Actors,
          Language: movieInfo.Language,
          Country: movieInfo.Country,
          Poster: movieInfo.Poster,
          Type: movieInfo.Type,
          Production: movieInfo.Production,
        });
        return createMovie;
      }
      if (movieInfo.Error) {
        return movieInfo;
      }
      return undefined;
    });
};

const addRoom = (JsonData) => {
  if (JsonData.Name) {
    const findRoom = Rooms.findOne({ Name: JsonData.Name });
    return findRoom
      .then((roomFound) => {
        if (!roomFound) {
          const addRoomRequest = Rooms.create({
            Name: JsonData.Name,
            Description: JsonData.Description,
            Seats: JsonData.Seats,
            Price: JsonData.Price,
            Available: JsonData.Available,
          });
          return addRoomRequest;
        }
        return 409;
      });
  }
  return new Promise((resolve) => { resolve(null); });
};

const findIfOpen = (startingTime, endTime, roomName) => {
  const foundEvents = Events.find({ Start: { $gte: startingTime, $lte: endTime }, Room: roomName });
  return foundEvents;
};

const addEvent = (JsonData) => {
  const getRoomInfo = Rooms.findOne({ Name: JsonData.Room });
  return getRoomInfo
    .then((roomFound) => {
      if (roomFound) {
        return Promise.all([roomFound, Movies.findOne({ IMDB: JsonData.IMDB })]);
      }
      throw new Error('Wrong room reference!');
    })
    .then(([roomFound, movieFound]) => {
      if (movieFound) {
        const movieEnd = helper.getEndMovieTime(JsonData.Start, movieFound.Runtime);
        const eventEnd = helper.addMinutes(movieEnd, 30);
        const foundEvents = findIfOpen(new Date(JsonData.Start), eventEnd, roomFound.Name);
        return Promise.all([roomFound, movieFound, foundEvents, movieEnd, eventEnd]);
      }
      throw new Error('Wrong movie reference');
    })
    .then(([roomFound, movieFound, foundEvents, movieEnd, eventEnd]) => {
      if (foundEvents.length === 0) {
        const createEvent = Events.create({
          Code: JsonData.Code,
          Room: roomFound.Name,
          Start: new Date(JsonData.Start),
          MovieEnd: movieEnd,
          EventEnd: eventEnd,
          MovieId: movieFound.IMDB,
          Price: JsonData.Price,
          Seats: JsonData.Seats ? JsonData.Seats : roomFound.Seats,
          Note: JsonData.Note,
        });
        return createEvent;
      }
      throw new Error('Spot is not available');
    });
};

module.exports = {
  getUser,
  addUser,
  addToken,
  delToken,
  addMovie,
  addRoom,
  addEvent,
};
