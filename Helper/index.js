require('dotenv').config();

const { APIURL, APIKEY } = process.env;
const axios = require('axios');
const moment = require('moment');

const dateRegEx = /^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])T[0,1][0-9]:(60|[0-5][0-9][+,-])(0[1-9]|1[0-4]):00)/;
const getMovie = (query) => {
  const request = axios.get(APIURL, {
    params: {
      t: query,
      apikey: APIKEY,
    },
  });
  return request;
};

const getMovieById = (movieId) => {
  const request = axios.get(APIURL, {
    params: {
      i: movieId,
      apikey: APIKEY,
    },
  });
  return request;
};

const validateDate = (date) => {
  const validated = dateRegEx.test(date);
  return validated;
};

function getEndMovieTime(dateStart, movieRuntime) {
  const dateEnd = moment(dateStart).add(movieRuntime, 'minutes').toDate();
  return dateEnd;
}

function addMinutes(dateStart, minutes) {
  const dateEnd = moment(dateStart).add(minutes, 'minutes').toDate();
  return dateEnd;
}

module.exports = {
  getMovie,
  getMovieById,
  validateDate,
  getEndMovieTime,
  addMinutes,
};
